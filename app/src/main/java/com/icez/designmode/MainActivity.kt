package com.icez.designmode

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.icez.designmode.databinding.RvItemBinding
import com.icez.recycleradapter.RecyclerAdapter
import com.icez.recycleradapter.holder.BaseItemTouchHelper
import com.icez.recycleradapter.holder.RecyclerViewHolder

class MainActivity : AppCompatActivity() {
    private var rv: RecyclerView? = null
    val list = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv = findViewById(R.id.rv)
        val mGridLayoutManager = GridLayoutManager(this, 2)
        rv?.layoutManager = LinearLayoutManager(this)
        mGridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                if (position == 0) {
                    return 2
                } else if (position == 1) {
                    return 1
                } else if (position == 3) {
                    return 2
                } else {
                    return 1
                }
            }

        }


        for (i in 0..10) {
            list.add("测试" + i)
        }
        val adapter = object : RecyclerAdapter<String,RvItemBinding>( R.layout.rv_item, list) {

            override fun onBusinessOperation(
                binding: RvItemBinding,
                position: Int,
                items: String?
            ) {
                binding?.rvItemGet?.setOnClickListener {
                    Log.e("icez", "数据：" + mData.toString())


                }
                binding?.rvItemTv?.text = mData?.get(position) ?: "not found"
//                BaseItemTouchHelper.instance.setClickDragForView(binding?.rvItemTv)
            }
        }

        rv?.adapter = adapter

        BaseItemTouchHelper.instance.initDragRecyclerSort(rv, adapter as RecyclerView.Adapter<RecyclerView.ViewHolder>, list)
    }
}