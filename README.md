# RecyclerAdapter

#### 介绍
RecyclerView通用适配器

#### 开发原因
由于Android自带的过于麻烦，每次都是需要实现多余且重复的代码；而这个则是把重复代码提取出去，只需要关注业务即可

#### 支持功能

1. 支持所有的RecyclerView使用
2. 支持多布局使用

#### 项目使用

引入项目

```groovy

```

