package com.icez.recycleradapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.icez.recycleradapter.holder.RecyclerViewHolder

/**
 * @sample 适配器
 * @param mContext 上下文
 * @param layoutResId item布局
 * @param mData 数据
 */
abstract class RecyclerAdapter<T,VB:ViewDataBinding>( val layoutResId:Int,val mData:ArrayList<T>?): RecyclerView.Adapter<RecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val binding = DataBindingUtil.inflate<VB>(LayoutInflater.from(parent.context),layoutResId,parent,false)
        return RecyclerViewHolder(binding.root,binding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        onBusinessOperation(holder.binding as VB,position,mData?.get(position))
    }

    override fun getItemCount(): Int {
        return mData?.size?:0
    }

    /**
     * @sample 业务操作
     * @param holder viewholder
     * @param position 当前索引
     */
    abstract fun onBusinessOperation(binding: VB, position: Int,items:T?)
}