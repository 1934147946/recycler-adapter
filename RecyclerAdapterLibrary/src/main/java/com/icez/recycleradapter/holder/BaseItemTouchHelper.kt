package com.icez.recycleradapter.holder

import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.icez.recycleradapter.RecyclerAdapter
import java.util.*

/**
 * @sample item帮助
 * @author icez
 */
class BaseItemTouchHelper {
    companion object {
        val instance: BaseItemTouchHelper by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            BaseItemTouchHelper()
        }
        var mItemTouchHelper: ItemTouchHelper? = null
    }

    /**
     * @sample 设置点击拖动的view
     * @param view
     * @param viewHolder
     */
    public fun setClickDragForView(view: View?, viewHolder: RecyclerView.ViewHolder) {
        view?.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    mItemTouchHelper?.startDrag(viewHolder)
                }
                else -> {

                }
            }
            return@setOnTouchListener false
        }
    }

    /**
     * @sample 拖动排序
     * @param rv 控件
     * @param datas 原数据
     * @param isLongPressDragEnabled 是否长按拖动
     * @param mGetDragVisibleDatas 获取拖动的数据；例如：val t = it as RecyclerViewHolder<*>；return@test t.mDataItems as String
     */
    public fun <M> initDragRecyclerSort(
        rv: RecyclerView?,
        adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>?,
        datas: ArrayList<M>?,
        isLongPressDragEnabled: Boolean = false
    ) {
        mItemTouchHelper = null
        mItemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.Callback() {
            override fun getMovementFlags(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                //控制拖拽的方向（一般是上下左右）
                //控制拖拽的方向（一般是上下左右）
                val dragFlags =
                    ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                //控制快速滑动的方向（一般是左右）ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                //控制快速滑动的方向（一般是左右）
                val swipeFlags = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
                return makeMovementFlags(
                    dragFlags,
                    swipeFlags
                )
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                val fromIndex = viewHolder.adapterPosition
                val toIndex = target.adapterPosition
                Collections.swap(datas,fromIndex,toIndex)
                adapter?.notifyItemMoved(
                    fromIndex,
                    toIndex
                )
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            }

            override fun isItemViewSwipeEnabled(): Boolean {
                return false
            }


            override fun isLongPressDragEnabled(): Boolean {
                return isLongPressDragEnabled
            }

            override fun clearView(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ) {
                super.clearView(recyclerView, viewHolder)
                adapter?.notifyDataSetChanged()
            }

        })

        mItemTouchHelper?.attachToRecyclerView(rv)

    }
}