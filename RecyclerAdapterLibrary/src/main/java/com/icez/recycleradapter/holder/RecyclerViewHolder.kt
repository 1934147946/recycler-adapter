package com.icez.recycleradapter.holder

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * @sample 基类ViewHolder
 * @author icez
 * @param itemView 布局控件
 */
class RecyclerViewHolder(val itemView: View,val binding: ViewDataBinding):RecyclerView.ViewHolder(itemView) {


    /**
     * @sample 获取子控件
     * @param resId 子控件id
     */
    fun <T:View?> getView(resId:Int):T?{
        return itemView.findViewById<T>(resId) as T
    }
}